﻿using Newtonsoft.Json;

namespace Canine.Net.Infrastructure.Storage
{
    [JsonConverter(typeof(OperatorConverter))]
    public abstract class Operator
    {
        public virtual void Validate() {; }
    }
}
