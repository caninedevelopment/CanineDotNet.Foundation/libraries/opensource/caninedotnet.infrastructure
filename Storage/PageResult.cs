﻿using System;
using System.Collections.Generic;

namespace Canine.Net.Infrastructure.Storage
{
    public interface IPageResult<TEntity> where TEntity : IBaseEntity
    {
        UInt64 NumberOfResults { get; set; }
        uint PageSize { get; set; }
        uint PageNumber { get; set; }
        IEnumerable<TEntity> Results { get; set; }
    }

    public interface IInputList<TEntity> where TEntity : class
    {
        IEnumerable<TEntity> List { get; set; }
    }
}
