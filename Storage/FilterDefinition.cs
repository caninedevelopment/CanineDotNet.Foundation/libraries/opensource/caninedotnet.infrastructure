﻿namespace Canine.Net.Infrastructure.Storage
{
    using Monosoft.Common.Command.Interfaces;

    public interface IFilterDefinition : IDtoRequest
    {
        StatementOperator Operators { get; set; }//TODO: tidligere blev serialisering/deserialisering issue løst med nedenstående, hvordan sikre vi os at det fortsat fungere?
    }
}
