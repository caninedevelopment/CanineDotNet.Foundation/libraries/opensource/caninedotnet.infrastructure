﻿namespace Canine.Net.Infrastructure.Storage
{
    using Monosoft.Common.Command.Interfaces;

    public enum OrderDirection
    {
        Ascending,
        Descending
    }

    public interface IPagingDefinition : IDtoRequest
    {
        uint PageSize { get; set; }
        uint PageNumber { get; set; }

        string OrderByField { get; set; }
        OrderDirection OrderByDirection { get; set; }
    }
}
