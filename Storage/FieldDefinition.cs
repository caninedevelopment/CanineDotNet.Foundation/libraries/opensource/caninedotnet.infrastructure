﻿using Monosoft.Common.Command.Interfaces;
using System.Collections.Generic;

namespace Canine.Net.Infrastructure.Storage
{
    public interface IFieldDefinition : IDtoRequest
    {
        List<string> FieldNames { get; set; }
    }
}
