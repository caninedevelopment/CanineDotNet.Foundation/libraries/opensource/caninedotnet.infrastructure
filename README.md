## Introduction
These common libs represents the dotnet core C# implementation of the ITutil microservice platform.
The are avaiable as nuget packages at https://gitlab.com/api/v4/projects/17035152/packages/nuget/index.json 

# Reference links

- [GitLab packages](https://gitlab.com/api/v4/projects/17035152/packages/nuget/index.json)
- [ITutil homepage](https://www.monosoft.dk/itutil/)

## Update nuget

Add Monosoft as source:

nuget source Add -Name Monosoft -Source "https://gitlab.com/api/v4/projects/21192147/packages/nuget/index.json" -UserName xxxxxx -Password xxxxxx


Pleaes update the packages with the following commands:

nuget push ./bin/Debug/Canine.Net.Infrastructure.4.0.0-preview.nupkg -Source Monosoft


