﻿//namespace Canine.Net.Infrastructure.Config
//{
//    using System.Collections.Generic;
//    public class NoSQLSettings
//    {
//        public List<string> Servers { get; set; }
//        public string DatabasePrefix { get; set; }
//        public string Username { get; set; }
//        public string Password { get; set; }
//        public string ApiKey { get; set; }
//        public NoSQLType ServerType { get; set; }

//        public string GetFullDBName(string databasename)
//        {
//            var prefix = DatabasePrefix ?? "itutil";
//            if (prefix.EndsWith("_") == false)
//            {
//                prefix += "_";
//            }

//            return (prefix + databasename).ToLower();
//        }

//        public string ConnectionString()
//        {
//            switch (this.ServerType)
//            {
//                case NoSQLType.MONGODB:
//                    string encodedCredentials = "";
//                    if (!string.IsNullOrEmpty(this.Username) && !string.IsNullOrEmpty(this.Password))
//                    {
//                        encodedCredentials = System.Web.HttpUtility.UrlEncode(this.Username) + ":" + System.Web.HttpUtility.UrlEncode(this.Password) + "@";
//                    }
//                    return $"mongodb://{encodedCredentials}{string.Join(",", this.Servers)}";
//                default:
//                    return "";
//            }
//        }

//        public enum NoSQLType
//        {
//            MONGODB
//        }
//    }
//}