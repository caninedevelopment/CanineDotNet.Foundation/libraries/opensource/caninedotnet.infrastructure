﻿//namespace Canine.Net.Infrastructure.Config
//{
//    using Newtonsoft.Json.Linq;
//    using System;
//    using System.Collections.Generic;
//    using System.IO;
//    public class GlobalRessources
//    {
//        public Dictionary<string, JObject> Configs { get; set; }

//        public static GlobalRessources GetConfig(string settingsFilename = null)
//        {
//            string filename = Directory.GetCurrentDirectory() + "/" + (string.IsNullOrEmpty(settingsFilename) ? "appsettings.json" : settingsFilename);

//            if (File.Exists(filename))
//            {
//                string json = File.ReadAllText(filename);
//                var globalResources = Newtonsoft.Json.JsonConvert.DeserializeObject<GlobalRessources>(json);

//                return globalResources;
//            }

//            return null;
//        }

//        /// <summary>
//        /// please use ITUtil.Config.Implementation
//        /// </summary>
//        /// <typeparam name="T"></typeparam>
//        /// <returns></returns>
//        public T GetSetting<T>()
//        {
//            return this.Configs[typeof(T).Name].ToObject<T>();
//        }
//    }
//}
