﻿using System;
using System.Threading.Tasks;
using Monosoft.Common;
using Canine.Net.Infrastructure.RabbitMQ.DTO;
using Canine.Net.Infrastructure.RabbitMQ.Event;
using Canine.Net.Infrastructure.RabbitMQ.Message;
using Canine.Net.Infrastructure.Console;
using System.Collections.Generic;
using System.Linq;

namespace Canine.Net.Infrastructure.Console
{
    public class ServiceMessageHandlers
    {
        public ServiceMessageHandlers(string servicename, ProgramVersion programVersion, List<IResource> resources)
        {
            this.Servicename = servicename;
            this.Resources = resources;
            this.ProgramVersion = programVersion;
        }
        private string Servicename { get; }
        private List<IResource> Resources { get;}
        private ProgramVersion ProgramVersion { get; }

        public async Task<ReturnMessage> HandleMessage(string[] topicparts, string messageAsJson, TokenInfo tokenInfo, DateTime messageTimestamp, string initiatingIP)
        {
            var operation = topicparts[1];
            if (operation == "help")
            {
                var messagewrapper = new ReturnMessage(true, GetHelpDTO(), LocalizedString.OK);
                EventClient.Instance.RaiseEvent("servicediscovery.help", messagewrapper);
                return messagewrapper;
            }
            else
            {
                return new ReturnMessage(true, null, LocalizedString.UnknownOperation);
            }
        }

        public ServiceDescriptors GetHelpDTO()
        {
            //TODO: Opdater ServiceDescriptors til eget format (minus swagger her!)
            ServiceDescriptors serviceDescriptor = new ServiceDescriptors();
            serviceDescriptor.Services.Add(new ServiceDescriptor()
            {
                Version = ProgramVersion,
                ResourceDescriptions = Resources.Select(resource => new ResourceDescription(resource)).ToList(),
                Name = Servicename,
            });

            //serviceDescriptor.Services.Add(new ServiceDescriptor(
            //        ProgramVersion,
            //        new ResourceDescription
            //        {
            //            Name = "",
            //            OperationDescriptions = new List<OperationDescription> {
            //                new OperationDescription() { Operation = "claims", Description = "returns claims defined by the service" }
            //            }
            //        }
            //        ));

            return serviceDescriptor;
        }
    }
}
