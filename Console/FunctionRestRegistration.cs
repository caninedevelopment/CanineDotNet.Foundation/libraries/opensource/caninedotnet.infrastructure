﻿namespace Canine.Net.Infrastructure.Console
{
    using Monosoft.Common.Command.Interfaces;
    using Canine.Net.Infrastructure.Command;

    internal class FunctionRestRegistration<TCommandModelIn, TCommandModelOut> : CommandRestRegistration, ICommand
        where TCommandModelIn : IDtoRequest
        where TCommandModelOut : IDtoResponse
    {
        public FunctionRestRegistration(IFunction<TCommandModelIn, TCommandModelOut> command, OperationType operationType, string name, string description, params Claim[] claims)
            : base(typeof(TCommandModelIn), typeof(TCommandModelOut), command, operationType, name, description, claims)
        {
        }
    }

    internal class FunctionRestRegistration<TCommandModelOut> : CommandRestRegistration, ICommand
    where TCommandModelOut : IDtoResponse
    {
        public FunctionRestRegistration(IFunction<TCommandModelOut> command, OperationType operationType, string name, string description, params Claim[] claims)
            : base(null, typeof(TCommandModelOut), command, operationType, name, description, claims)
        {
        }
    }
}
