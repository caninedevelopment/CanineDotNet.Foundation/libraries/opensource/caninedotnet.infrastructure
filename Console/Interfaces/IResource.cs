﻿namespace Canine.Net.Infrastructure.Console
{
    using Monosoft.Common.Command.Interfaces;
    using Canine.Net.Infrastructure.Command;
    using System.Collections.Generic;

    public interface IResource
    {
        string Name { get; }
        List<CommandRestRegistration> Commands { get; }
        List<IEventDefinitionBase> Events { get; }
        void AddCommand<TCommandModelIn, TCommandModelOut>(IFunction<TCommandModelIn, TCommandModelOut> command, OperationType operationType, string name, string description, params Claim[] claims) where TCommandModelIn : IDtoRequest where TCommandModelOut : IDtoResponse;
        void AddCommand<TCommandModelOut>(IFunction<TCommandModelOut> command, OperationType operationType, string name, string description, params Claim[] claims) where TCommandModelOut : IDtoResponse;
        void AddCommand<TCommandModelIn>(IProcedure<TCommandModelIn> command, OperationType operationType, string name, string description, params Claim[] claims) where TCommandModelIn : IDtoRequest;
        void AddCommand(IProcedure command, OperationType operationType, string name, string description, params Claim[] claims);
    }
}
