﻿namespace Canine.Net.Infrastructure.Console
{
    using Monosoft.Common.Command.Interfaces;
    using System.Collections.Generic;
    using Canine.Net.Infrastructure.Command;
    using Canine.Net.Infrastructure.RabbitMQ.DTO;

    public abstract class BaseResource : IResource
    {
        protected string name;
        protected List<CommandRestRegistration> commands;
        protected List<IEventDefinitionBase> events;
        protected BaseResource(string name)
        {
            this.name = name.ToLower();
            commands = new List<CommandRestRegistration>();
            events = new List<IEventDefinitionBase>();
        }

        public string Name { get { return this.name; } }
        public List<CommandRestRegistration> Commands { get { return commands; } }

        public void AddCommand<TCommandModelIn, TCommandModelOut>(IFunction<TCommandModelIn, TCommandModelOut> command, OperationType operationType, string name, string description, params Claim[] claims) where TCommandModelIn : IDtoRequest where TCommandModelOut : IDtoResponse
        {
            this.commands.Add(new FunctionRestRegistration<TCommandModelIn, TCommandModelOut>(command, operationType, name, description, claims));
        }
        public void AddCommand<TCommandModelOut>(IFunction<TCommandModelOut> command, OperationType operationType, string name, string description, params Claim[] claims) where TCommandModelOut : IDtoResponse
        {
            this.commands.Add(new FunctionRestRegistration<TCommandModelOut>(command, operationType, name, description, claims));
        }

        public void AddCommand<TCommandModelIn>(IProcedure<TCommandModelIn> command, OperationType operationType, string name, string description, params Claim[] claims) where TCommandModelIn : IDtoRequest
        {
            this.commands.Add(new ProcedureRestRegistration<TCommandModelIn>(command, operationType, name, description, claims));
        }
        public void AddCommand(IProcedure command, OperationType operationType, string name, string description, params Claim[] claims)
        {
            this.commands.Add(new ProcedureRestRegistration(command, operationType, name, description, claims));
        }

        public List<IEventDefinitionBase> Events
        {
            get
            {
                var res = new List<IEventDefinitionBase>();
                res.AddRange(events);

                res.Add(new EventDefinition<EventCallback>("servicediscovery.help", $"This event is raised as a response to {this.name}.help"));

                //foreach (var cmd in commands)
                //{
                //    res.Add(new EventDefinition<EventCallback>($"{this.name}.v{this.ProgramVersion.Major}.{cmd.Name.ToLowerInvariant()}.succeeded", "This event is raised every time the command was successfully executed"));
                //    res.Add(new EventDefinition<System.String>($"{this.name}.v{this.ProgramVersion.Major}.{cmd.Name.ToLowerInvariant()}.failed", "This event is raised when a command failed on execution"));
                //}

                return events;
            }
        }
    }
}
