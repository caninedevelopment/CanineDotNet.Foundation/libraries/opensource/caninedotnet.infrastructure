﻿using Monosoft.Common.Command;
using Canine.Net.Infrastructure.RabbitMQ;
using Canine.Net.Infrastructure.RabbitMQ.Common.Event;
using Canine.Net.Infrastructure.RabbitMQ.Event;
using Canine.Net.Infrastructure.RabbitMQ.Message;
using Canine.Net.Infrastructure.RabbitMQ.Request;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Canine.Net.Infrastructure.Console;
using System.Globalization;

namespace Canine.Net.Infrastructure.Console
{
    public static class Program
    {
        public static void StartRabbitMq(string servicename, ProgramVersion programVersion, List<IResource> namespaces, bool diagnostics = false)
        {
            StartRabbitMq(servicename, programVersion, namespaces, null, diagnostics);
        }

        public static Task StartRabbitMq(string servicename, ProgramVersion programVersion, List<IResource> resources, List<IEventListener> eventListeners, bool diagnostics = false)
        {
            var lowercaseServiceName = servicename.ToLower();
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };

            // start the request server (for handling incomming requests)
            List<MessageQueueConfiguration> configurations = new List<MessageQueueConfiguration>();

            if (eventListeners != null)
            {
                foreach (var Instance in eventListeners)
                {
                    configurations.Add(new EventConfiguration(
                    lowercaseServiceName,/* Instance.ServiceName.ToLower()*/
                    new List<string>() { lowercaseServiceName/* Instance.ServiceName.ToLower()*/ + ".#" },
                    new Constants.EventHandler(Instance.Handler)
                    ));
                }
            }

            var serviceMessageHandler = new ServiceMessageHandlers(servicename, programVersion, resources);
            foreach (var Resource in resources)
            {
                configurations.Add(
                    new RequestConfiguration(//for handling routes that are noncross-version (such as the service itself)
                        lowercaseServiceName + $"_v{programVersion.Major}",
                        Resource.Commands.Select(p => $"{lowercaseServiceName}.v{programVersion.Major}.{Resource.Name.ToLower(CultureInfo.InvariantCulture)}.{p.Name.ToLower(CultureInfo.InvariantCulture)}").ToList(),
                        new ResourceMessageHandler(Resource).HandleMessage)
                    );
            }

            //configurations.Add(new RequestConfiguration(//for handling routes that are cross-version (such as help and service discovery)
            //    lowercaseServiceName + "_help",
            //    new List<string>() { lowercaseServiceName/*servicename*/  + ".help"/*, servicename + ".scarfoldTS"*/ },
            //    new ServiceMessageHandler(resources).HandleMessage,
            //    true)
            //    );

            configurations.Add(new RequestConfiguration(//for handling routes that are cross-version (such as help and service discovery)
                lowercaseServiceName+ "_servicediscovery_help",
                new List<string>() { "servicediscovery.help" },
                new ServiceMessageHandler(resources).HandleMessage,
                true)
                );

            //configurations.Add(new RequestConfiguration(//for handling routes that are noncross-version (such as the service itself)
            //lowercaseServiceName/*servicename*/  + $".v{programVersion.Major}",
            //new List<string>() { lowercaseServiceName/*servicename*/  + $".v{programVersion.Major}" + ".claims" },
            //new ServiceMessageHandler(resources).HandleMessage)
            //);

            //configurations.Add(new RequestConfiguration(//for handling routes that are noncross-version (such as the service itself)
            //lowercaseServiceName/*servicename*/  + $".v{programVersion.Major}",
            //new List<string>() { lowercaseServiceName/*servicename*/  + $".v{programVersion.Major}" + ".eventCallback" },
            //new ServiceMessageHandler(resources).HandleMessage)
            //);

            var helpdto = new ServiceMessageHandlers(lowercaseServiceName/*servicename*/, programVersion , resources).GetHelpDTO();
            EventClient.Instance.RaiseEvent("servicediscovery.register", new ReturnMessage(true, helpdto));

            System.Console.WriteLine("Starting request server");
            using (var server = new RequestServer(configurations, diagnostics))
            {
                while (true)
                {
                    Thread.Sleep(1);
                }
            }
        }
    }
}
