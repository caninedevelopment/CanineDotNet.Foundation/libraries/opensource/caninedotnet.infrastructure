﻿namespace Canine.Net.Infrastructure.Console
{
    using Monosoft.Common.Command.Interfaces;
    using Canine.Net.Infrastructure.Command;
    using Canine.Net.Infrastructure.Console;

    internal class ProcedureRestRegistration<TCommandModelIn> : CommandRestRegistration, ICommand
        where TCommandModelIn : IDtoRequest
    {
        public ProcedureRestRegistration(IProcedure<TCommandModelIn> command, OperationType operationType, string name, string description, params Claim[] claims)
            : base(typeof(TCommandModelIn), null, command, operationType, name, description, claims)
        {
        }
    }

    internal class ProcedureRestRegistration : CommandRestRegistration, ICommand
    {
        public ProcedureRestRegistration(IProcedure command, OperationType operationType, string name, string description, params Claim[] claims)
            : base(null, null, command, operationType, name, description, claims)
        {
        }
    }
}
