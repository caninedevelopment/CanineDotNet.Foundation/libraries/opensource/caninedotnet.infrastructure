﻿namespace Canine.Net.Infrastructure.Console
{
    public enum OperationType
    {
        Event,
        Get,
        Update,
        Insert,
        Delete,
        Execute
    }
}
