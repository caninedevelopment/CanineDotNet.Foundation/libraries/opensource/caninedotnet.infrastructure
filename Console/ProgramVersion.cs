﻿namespace Canine.Net.Infrastructure.Console
{
    public class ProgramVersion
    {
        public ProgramVersion()
        {
        }

        public ProgramVersion(int major)
        {
            this.Major = major;
        }

        public string Version
        {
            get
            {
                return string.Format($"{this.Major}"); 
            }

            set
            {
                this.Major = int.Parse(value);
            }
        }

        public int Major { get; set; }
    }
}
