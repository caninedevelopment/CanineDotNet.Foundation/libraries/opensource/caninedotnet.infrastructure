﻿namespace Canine.Net.Infrastructure.RabbitMQ.Request
{
    using System;
    using System.Collections.Generic;
    using global::RabbitMQ.Client;
    using global::RabbitMQ.Client.Events;
    using System.Linq;
    using System.Net.Http;
    using Monosoft.Common;
    using System.Threading.Tasks;
    using Canine.Net.Infrastructure.RabbitMQ.Event;
    using Canine.Net.Infrastructure.RabbitMQ.Message;
    using Monosoft.Common.Exceptions;
    using Canine.Net.Infrastructure.RabbitMQ.DTO;
    using Canine.Net.Infrastructure.Config;
    using Canine.Net.Infrastructure.Console;

    /// <summary>
    /// The request server is used to consume messages that are issued to the request exchange
    /// </summary>
    public class RequestServer : IDisposable
    {
        public IConnection connection = null;
        /// <summary>
        /// Initializes a new instance of the <see cref="RequestServer"/> class.
        /// Starts the request server, so that it will consume messages from the request (ms.request) exchange
        /// </summary>
        /// <param name="settings">List of topic/routes that are consumed by this server</param>
        public RequestServer(List<MessageQueueConfiguration> settings, bool diagnostics = false/*, double? automaticShutdownSeconds = null*/)
        {
            var config = new RabbitMQSettings()
            {
                  Server = Environment.GetEnvironmentVariable("rabbitmq_host"),
                  Port = Environment.GetEnvironmentVariable("rabbitmq_port"),
                  Username = Environment.GetEnvironmentVariable("rabbitmq_username"),
                  Password = Environment.GetEnvironmentVariable("rabbitmq_password"),
            };

            var factory = new ConnectionFactory()
            {
                Port = config.Port.Length > 1 ? int.Parse(config.Port) : 5672,
                HostName = config.Server,
                UserName = config.Username,
                Password = config.Password
            };

            factory.AutomaticRecoveryEnabled = true;
            factory.NetworkRecoveryInterval = TimeSpan.FromSeconds(10);

            connection = factory.CreateConnection();
            var channel = connection.CreateModel();
            {
                channel.BasicQos(0, 1, false);

                foreach (var setting in settings)
                {
                    #region Request logic
                    channel.ExchangeDeclare(exchange: setting.ExchangeName, type: "topic");

                    var queuename = "ms.queue." + setting.QueueName;
                    System.Console.WriteLine("Declaring queue: " + queuename);

                    channel.QueueDeclare(queue: queuename, durable: true, exclusive: false, autoDelete: false, arguments: null);
                    foreach (var routekey in setting.RoutingKeys)
                    {
                        channel.QueueBind(queuename, setting.ExchangeName, routekey);
                    }

                    var consumer = new EventingBasicConsumer(channel);

                    consumer.Received += (model, ea) =>
                    {
                        ReturnMessage response = null;
                        var body = ea.Body;
                        var props = ea.BasicProperties;
                        var replyProps = channel.CreateBasicProperties();
                        replyProps.CorrelationId = props.CorrelationId;
                        replyProps.Headers = props.Headers;

                        RabbitMqHeader ItUtil_header = null;

                        try
                        {
                            if (ea.BasicProperties.Headers != null)
                            {
                                var ItUtil_header_string = System.Text.Encoding.UTF8.GetString(ea.BasicProperties.Headers["ITUtil"] as byte[]);
                                ItUtil_header = Newtonsoft.Json.JsonConvert.DeserializeObject<RabbitMqHeader>(ItUtil_header_string);
                            }

                            if (setting is EventConfiguration)
                            {
                                if (diagnostics) System.Console.WriteLine("Receiving event: " + ea.RoutingKey);
                                MessageFlow.HandleEvent(/*setting.QueueName,*/ ea, (setting as EventConfiguration)?.Handler);
                            }

                            if (setting is RequestConfiguration)
                            {
                                if (diagnostics) System.Console.WriteLine("Receiving request: " + ea.RoutingKey);
                                var s = setting as RequestConfiguration;

                                var messageAsJson = System.Text.Encoding.UTF8.GetString(ea.Body.ToArray());

                                List<string> topicparts = ea.RoutingKey.Split('.').ToList();
                                //if (!s.IsServiceHandler) //i.e. we expect version number to be part of the call
                                //{
                                //    topicparts.RemoveAt(1);//remove version number...
                                //}
                                response = HandleMessage(ItUtil_header, messageAsJson, topicparts, s.Handler/*, ItUtil_header == null ? "" : ItUtil_header.Ip*/).Result;
                            }
                        }
                        finally
                        {
                            channel.BasicAck(ea.DeliveryTag, false);
                            if (setting is RequestConfiguration)
                            {
                                if (props?.ReplyTo != null)
                                {//RESPONSE TO RPC CALLS
                                    Console.WriteLine("RESPONDING");
                                    var responseByteArray = System.Text.Encoding.UTF8.GetBytes(Newtonsoft.Json.JsonConvert.SerializeObject(response));
                                    channel.BasicPublish(exchange: string.Empty, routingKey: props.ReplyTo, basicProperties: replyProps, body: responseByteArray);
                                }
                                else
                                {//RESPONSE TO FAF CALLS
                                    if (response != null && ItUtil_header != null)
                                    {
                                        response.ResponseToClientId = ItUtil_header.ClientId;
                                        response.ResponseToMessageId = ItUtil_header.MessageId;
                                        EventClient.Instance.RaiseEvent(ea.RoutingKey, response);
                                    }
                                }
                            }
                        }
                    };

                    channel.BasicConsume(queuename, false, consumer);
                    #endregion
                }
            }
        }

        private static async Task<ReturnMessage> HandleMessage(RabbitMqHeader header, string messageAsJson, List<string> topicparts, MessageFlow.MessageHandler handler/*, string initiatedByIp*/)
        {
            ReturnMessage returnObj = null;

            try
            {
                if (handler != null)
                {
                    if (header.IsDirectLink)
                    {
                        var directLink = Newtonsoft.Json.JsonConvert.DeserializeObject<DirectLink>(messageAsJson);
                        HttpClient httpclient = new HttpClient();
                        var response = httpclient.GetStringAsync(directLink.url).Result;
                        messageAsJson = response;
                    }
                    int parts = topicparts.Count;
                    string service = parts > 0 ? topicparts[0] : "";
                    ProgramVersion version = parts > 1 ? new ProgramVersion(int.Parse(topicparts[1].Substring(1))) : new ProgramVersion(1);
                    string resource = parts > 2 ? topicparts[2] : "";
                    string operation = parts > 3 ? topicparts[3] : "";

                    returnObj = await handler(service, version, resource, operation, messageAsJson, header.TokenInfo, header.MessageIssueDate, header.Ip).ConfigureAwait(false);
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null && ex.InnerException is LocalizedException)
                {
                    //TODO: find InnerException type som de kendte (common) LocalizedExceptions, og sæt errorcode derfra
                    var lex = ex.InnerException as LocalizedException;
                    var exceptionDetails = new ReturnMessage(false, lex, lex.Error);
                    EventClient.Instance.RaiseEvent(topicparts.ToArray() + ".failed", exceptionDetails);
                    return exceptionDetails;
                }
                else
                {
                    var trace = new Trace(string.Join(".", topicparts), messageAsJson, ex.GetExceptionAsReportText());
                    var exceptionDetails = new ReturnMessage(false, trace, LocalizedString.Error);
                    EventClient.Instance.RaiseEvent(topicparts.ToArray() + ".failed", exceptionDetails);
                    return exceptionDetails;
                }
            }

            return returnObj;
        }

        /// <summary>
        /// Dispose of the rabbitMQ connection
        /// </summary>
        public void Dispose()
        {
            connection.Dispose();
        }
    }
}
