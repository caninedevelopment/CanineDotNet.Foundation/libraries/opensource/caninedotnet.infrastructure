﻿
namespace Canine.Net.Infrastructure.RabbitMQ
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Monosoft.Common;
    using Canine.Net.Infrastructure.Console;
    using Canine.Net.Infrastructure.RabbitMQ.DTO;
    using Canine.Net.Infrastructure.RabbitMQ.Message;

    public class ServiceMessageHandler
    {
        public ServiceMessageHandler(List<IResource> resources)
        {
            this.Resources = resources;
        }
        private List<IResource> Resources { get; }
        public async Task<ReturnMessage> HandleMessage(string service, ProgramVersion version, string resource, string operation, string messageAsJson, TokenInfo tokenInfo, DateTime messageTimestamp, string initiatedbyIp)
        {
            //if (operation == "claims")
            //{
            //    List<Command.Claim> definitions = new List<Command.Claim>();

            //    foreach (var res in Resources)
            //        foreach (var cmd in res.Commands) //instance.OperationDescription)
            //        {
            //            foreach (var claimDefinition in cmd.Claims)
            //            {
            //                if (!definitions.Any(p => p.key == claimDefinition.key))
            //                {
            //                    definitions.Add(claimDefinition);
            //                }
            //            }
            //        }
            //    ClaimDefinitions claims = new ClaimDefinitions(definitions);

            //    return new ReturnMessage(true, new ClaimResult() { Claims = claims, Version = version }, LocalizedString.OK);
            //}

            if (operation == "eventCallback")
            {
                var baseDir = System.IO.Directory.GetCurrentDirectory() + "/events/";
                var request = Newtonsoft.Json.JsonConvert.DeserializeObject<EventCallback>(messageAsJson);
                var filename = baseDir + request.id.ToString() + ".event";
                if (System.IO.File.Exists(filename))
                {
                    var filedata = System.IO.File.ReadAllText(filename);
                    return Newtonsoft.Json.JsonConvert.DeserializeObject<ReturnMessage>(filedata);
                }
                else
                {
                    return new ReturnMessage(false, null, new LocalizedString(new Guid("32a94896-6d5d-4ef1-b88b-63875ceddfa0"), "Event data does not exists"));
                }
            }

            return new ReturnMessage(false, null, LocalizedString.UnknownOperation);
        }
    }
}
