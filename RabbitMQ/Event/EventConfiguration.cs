﻿
using Canine.Net.Infrastructure.RabbitMQ.Message;
using System.Collections.Generic;

namespace Canine.Net.Infrastructure.RabbitMQ.Event
{
    /// <summary>
    /// Configuration for event messages
    /// </summary>
    public class EventConfiguration : MessageQueueConfiguration
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EventConfiguration"/> class.
        /// </summary>
        /// <param name="queueName">Name of the queue that the messages are to be placed in</param>
        /// <param name="routingKey">Name of the routing which messages are to be placed on the queue</param>
        /// <param name="handler">The message handler</param>
        public EventConfiguration(string queueName, List<string> routingKeys, Constants.EventHandler handler)
            : base(Constants.EventExchangeName, queueName, routingKeys)
        {
            this.Handler = handler;
        }

        /// <summary>
        /// Gets or sets the handler, for handling events
        /// </summary>
        public Constants.EventHandler Handler { get; set; }
    }
}
