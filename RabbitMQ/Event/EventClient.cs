﻿
namespace Canine.Net.Infrastructure.RabbitMQ.Event
{
    using System;
    using global::RabbitMQ.Client;
    using global::RabbitMQ.Client.Events;
    using Canine.Net.Infrastructure.Config;
    using Canine.Net.Infrastructure.RabbitMQ.Message;

    /// <summary>
    /// A client for sending events to the message queue
    /// </summary>
    public class EventClient
    {
        private static EventClient instance = null;

        private readonly IConnection connection;

        private readonly IModel channel;

        private readonly EventingBasicConsumer consumer;

        /// <summary>
        /// Initializes a new instance of the <see cref="EventClient"/> class.
        /// </summary>
        private EventClient()
        {
            var config = new RabbitMQSettings()
            {
                Server = Environment.GetEnvironmentVariable("rabbitmq_host"),
                Port = Environment.GetEnvironmentVariable("rabbitmq_port"),
                Username = Environment.GetEnvironmentVariable("rabbitmq_username"),
                Password = Environment.GetEnvironmentVariable("rabbitmq_password"),
            };
            var serverparts = config.Server.Split(':');
            var factory = new ConnectionFactory()
            {
                Port = serverparts.Length > 1 ? int.Parse(serverparts[1]) : 5672,
                HostName = serverparts[0],
                UserName = config.Username,
                Password = config.Password
            };
            factory.AutomaticRecoveryEnabled = true;
            factory.NetworkRecoveryInterval = TimeSpan.FromSeconds(10);
            this.connection = factory.CreateConnection();
            this.channel = this.connection.CreateModel();
            this.channel.ExchangeDeclare(exchange: Canine.Net.Infrastructure.RabbitMQ.Constants.EventExchangeName, type: "topic");
            this.consumer = new EventingBasicConsumer(this.channel);
        }

        ~EventClient()
        {
            this.connection.Close();
        }

        /// <summary>
        /// Gets the singleton instance of the client
        /// </summary>
        public static EventClient Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new EventClient();
                }

                return instance;
            }
        }

        /// <summary>
        /// Places an event on the message queue
        /// </summary>
        /// <param name="route">The topic/route of the event (ex. user.created)</param>
        /// <param name="eventdata">The event details</param>
        public void RaiseEvent(string route, ReturnMessage eventdata)
        {
            byte[] bsonMessage = System.Text.Encoding.UTF8.GetBytes(Newtonsoft.Json.JsonConvert.SerializeObject(eventdata));

            var properties = channel.CreateBasicProperties();
            properties.Persistent = true;
            channel.ExchangeDeclare(exchange: "ms.events", type: "topic");
            channel.BasicPublish(
                exchange: "ms.events",
                routingKey: route,
                basicProperties: properties,
                body: bsonMessage);
        }
    }
}