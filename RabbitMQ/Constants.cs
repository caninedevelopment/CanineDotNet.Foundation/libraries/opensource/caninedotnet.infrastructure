﻿using Canine.Net.Infrastructure.RabbitMQ.Message;

namespace Canine.Net.Infrastructure.RabbitMQ
{
    public static class Constants
    {
        public const string EventExchangeName = "ms.events";
        public const string RequestExchangeName = "ms.request";
        public const string QueuePrefix = "ms.queue.";
        public delegate void EventHandler(string[] topicparts, ReturnMessage data);
    }
}
