﻿namespace Canine.Net.Infrastructure.RabbitMQ.Message
{
    using System;
    using EventHandler = Constants.EventHandler;
    using global::RabbitMQ.Client.Events;
    using Canine.Net.Infrastructure.RabbitMQ.DTO;
    using Canine.Net.Infrastructure.RabbitMQ.Common;
    using System.Threading.Tasks;
    using Canine.Net.Infrastructure.Console;

    /// <summary>
    /// Class for handling the generic part of the messageflow, unwrapping the wrapper and handling diagnostics
    /// </summary>
    public static class MessageFlow
    {
        /// <summary>
        /// A delegate description for handling request messages
        /// </summary>
        /// <param name="topicparts">a list of route parts (ex: [user, create])</param>
        /// <param name="wrapper">The messagewrapper object</param>
        /// <returns>the returning object serialised as json and converted to utf8 byte array</returns>
        public delegate Task<ReturnMessage> MessageHandler(string service, ProgramVersion version, string resource, string operation, string messageAsJson, TokenInfo tokenInfo, DateTime messageTimestamp, string initiatedByIp);
        /// <summary>
        /// Handles the basic messageflow for events
        /// </summary>
        /// <param name="servicename">name of the calling service</param>
        /// <param name="ea">the rabbitMQ delivery event</param>
        /// <param name="handler">the message handler for handling specifik logic</param>
        public static void HandleEvent(/*string servicename, */BasicDeliverEventArgs ea, EventHandler handler)
        {
            string[] topicparts = ea.RoutingKey.Split('.');
            if (ea.Body.Length != 0)
            {
                var wrapper = Newtonsoft.Json.JsonConvert.DeserializeObject<ReturnMessage>(System.Text.Encoding.UTF8.GetString(ea.Body.ToArray()));
                handler?.Invoke(topicparts, wrapper);
            }
        }
    }
}