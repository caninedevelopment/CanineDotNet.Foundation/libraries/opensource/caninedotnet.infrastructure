﻿using Canine.Net.Infrastructure.Command;
using Canine.Net.Infrastructure.Console;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Canine.Net.Infrastructure.RabbitMQ.Message
{
    public class ResourceDescription
    {
        public ResourceDescription(IResource resource)
        {
            if(resource != null) {
                this.Name = resource.Name;
                this.OperationDescriptions = OperationDescription.Convert(resource);
            }
        }

        public string Name { get; set; }
        public List<OperationDescription> OperationDescriptions { get; set; }
    }
}
