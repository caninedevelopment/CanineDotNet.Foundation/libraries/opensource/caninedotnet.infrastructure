﻿using Monosoft.Common;
using System;

namespace Canine.Net.Infrastructure.Command
{
    public partial class Claim
    {
        /// <summary>
        /// User is administrator, i.e. can see all orders.
        /// </summary>
        public static Claim IsAdmininistrator = new Claim()
        {
            key = "IsAdmininistrator",
            dataContext = DataContextEnum.organisationClaims,
            description = new[]
            {
                new LocalizedString(new Guid("15c1e210-32a7-4f14-ab10-f8b11bbda809"),"User is system administrator, i.e. can setup the service"),
            },
        };
        public static Claim IsOperator = new Claim()
        {
            key = "IsOperator",
            dataContext = DataContextEnum.organisationClaims,
            description = new[]
            {
                new LocalizedString(new Guid("88b08ac7-e826-4eb1-99f5-6915d810cc8b"),"User is an operator, i.e. has full access to administate the services data"),
            },
        };
        public static Claim IsConsumer = new Claim()
        {
            key = "IsConsumer",
            dataContext = DataContextEnum.organisationClaims,
            description = new[]
            {
                new LocalizedString(new Guid("3f0bc871-79d7-4dbe-ac89-99022c97627e"),"User is a data consumer, i.e. has access read data"),
            },
        };
    }
}