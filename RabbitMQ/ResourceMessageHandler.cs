﻿namespace Canine.Net.Infrastructure.RabbitMQ
{
    using Canine.Net.Infrastructure.Console;
    using Canine.Net.Infrastructure.RabbitMQ.DTO;
    using Canine.Net.Infrastructure.RabbitMQ.Event;
    using Canine.Net.Infrastructure.RabbitMQ.Message;
    using Monosoft.Common;
    using Monosoft.Common.Command.Interfaces;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class ResourceMessageHandler
    {
        public ResourceMessageHandler(IResource instance)
        {
            this.Instance = instance;
        }

        private IResource Instance { get; }

        public void RaiseEvent(string eventname, ReturnMessage eventdata)
        {
            EventClient.Instance.RaiseEvent(eventname, eventdata);
        }

        public async Task<ReturnMessage> HandleMessage(string service, ProgramVersion version, string resource, string operation, string messageAsJson, TokenInfo tokenInfo, DateTime messageTimestamp, string initiatedbyIp)
        {
            //var routenamespace = topicparts[0];
            //var operation = topicparts[1];
            var command = Instance.Commands.Find(p => string.Equals(p.Name, operation, StringComparison.OrdinalIgnoreCase));
            if (command != null)
            {
                if (!CheckAccess(tokenInfo, messageTimestamp, command.Claims, Instance))
                {
                    string expecting = string.Join(" or ", command.Claims.Select(p => p.key));
                    return new ReturnMessage(false, null, LocalizedString.MissingCredentials /* + " : " + expecting*/);
                }
                else
                {
                    var baseDir = System.IO.Directory.GetCurrentDirectory() + "/events/";
                    var TTL = DateTime.Now.AddHours(-1);
                    Guid EventId = Guid.NewGuid();
                    string eventRoute = $"{service}.v{version.Major.ToString()}.{resource}.{operation}";
                    string callbackRoute = $"{service}.v{version.Major.ToString()}.eventCallback";

                    var t = command.Command.GetType();//redundant med linje 30, måske det skal slås op en gang for alle og gemmes i et dic?
                    var method = Array.Find(t.GetMethods(), p => p.Name == "Execute");

                    ReturnMessage result = null;

                    var handlemessageTask = new Task(() =>
                    {
                        bool isFunctionCmd = t.GetInterfaces()
                            .Any(x =>
                            x.IsGenericType &&
                            (
                            x.GetGenericTypeDefinition() == typeof(IFunction<,>) ||
                            x.GetGenericTypeDefinition() == typeof(IFunction<>)
                            ));
                        object[] input = null;
                        var hasInput = command.ModelIn != null;
                        if (hasInput)
                        {
                            var cmd_input_instance = Activator.CreateInstance(command.ModelIn);
                            if (!string.IsNullOrEmpty(messageAsJson))
                            {
                                Newtonsoft.Json.JsonConvert.PopulateObject(messageAsJson, cmd_input_instance);
                            }

                            if (cmd_input_instance is Monosoft.Common.Interfaces.IHeader)
                            {
                                Monosoft.Common.Interfaces.IHeader cmd_input_headerInfo = (cmd_input_instance as Monosoft.Common.Interfaces.IHeader);
                                if (cmd_input_headerInfo != null)
                                {
                                    cmd_input_headerInfo.InitiatedByIp = initiatedbyIp;
                                    cmd_input_headerInfo.Token = tokenInfo.TokenId.ToString();
                                    cmd_input_headerInfo.User = tokenInfo.UserId.ToString();
                                }
                            }

                            if (cmd_input_instance is IDtoRequest)
                            {
                                (cmd_input_instance as IDtoRequest)?.Validate();
                            }
                            input = new object[] { cmd_input_instance };
                        }

                        if (isFunctionCmd)
                        {
                            var resultObj = method.Invoke(command.Command, input);
                            result = new ReturnMessage(true, resultObj, LocalizedString.OK);
                        }
                        else
                        {
                            method.Invoke(command.Command, input);
                            result = new ReturnMessage(true, null, LocalizedString.OK);
                        }
                    });
                    handlemessageTask.Start();
                    await handlemessageTask.ConfigureAwait(false);

                    CleanupOldEventFiles(baseDir, TTL);
                    var eventwrapper = new ReturnMessage(new EventCallback() { id = EventId, route = callbackRoute });
                    System.IO.File.WriteAllText(baseDir + EventId.ToString() + ".event", Newtonsoft.Json.JsonConvert.SerializeObject(result));
                    eventwrapper.Success = true;
                    RaiseEvent(eventRoute + ".succeeded", eventwrapper);

                    return result;
                }
            }
            else
            {
                return new ReturnMessage(false, null, LocalizedString.UnknownOperation);
            }
        }

        private static void CleanupOldEventFiles(string baseDir, DateTime TTL)
        {
            if (!System.IO.Directory.Exists(baseDir))
            {
                System.IO.Directory.CreateDirectory(baseDir);
            }

            var di = new System.IO.DirectoryInfo(baseDir);
            foreach (var file in di.GetFileSystemInfos().ToList())
            {
                if (file.CreationTime < TTL)
                {
                    file.Delete();
                }
            }
        }

        private bool CheckAccess(TokenInfo tokenInfo, DateTime messageTimestamp, List<Command.Claim> requiredClaims, IResource instance)
        {
            bool hasAccess = true;
            if (requiredClaims?.Count() > 0)
            {
                hasAccess = false;
                foreach (var requiredClaim in requiredClaims)
                {
                    if (HasClaim(tokenInfo, messageTimestamp, requiredClaim, instance))
                    {
                        hasAccess = true;
                        break;
                    }
                }
            }
            return hasAccess;
        }

        /// <summary>
        /// Check if a given token has a given claim in a specific organisation
        /// </summary>
        /// <param name="token">The token to check</param>
        /// <param name="organisationContext">The organisation context to check</param>
        /// <param name="claim">The claim to check</param>
        /// <returns>true if the token has the claim</returns>
        public bool HasClaim(TokenInfo tokenInfo, DateTime messageTimestamp, Command.Claim claim, IResource instance)
        {
            if (tokenInfo == null)
            {
                return false;
            }
            if (tokenInfo.ValidUntil >= messageTimestamp)
            {
                if (tokenInfo.Claims == null)
                {
                    return false;
                }
                else
                {
                    return tokenInfo.Claims.Any(x => x.ToLower() == instance.Name.ToLower() + "." + claim.key.ToLower());
                }
            }
            else
            {
                return false;
            }
        }
    }

    public class ClaimResult
    {
        public ProgramVersion Version { get; set; }
        public ClaimDefinitions Claims { get; set; }
    }
}