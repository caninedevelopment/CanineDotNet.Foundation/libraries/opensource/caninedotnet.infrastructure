﻿using System;
using System.Collections.Generic;

namespace Canine.Net.Infrastructure.RabbitMQ.DTO
{
    /// <summary>
    /// TokenInfo description
    /// </summary>
    public class TokenInfo
    {
        /// <summary>
        /// Gets or sets Tokenid
        /// </summary>
        public Guid TokenId { get; set; }

        /// <summary>
        /// Gets or sets Userid
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// Gets or sets ValidUntil
        /// </summary>
        public DateTime ValidUntil { get; set; }

        /// <summary>
        /// Gets or sets Claims
        /// </summary>
        public List<string> Claims { get; set; }

        public bool IsValidToken()
        {
            return this.ValidUntil > DateTime.Now;
        }
    }
}
