﻿using Canine.Net.Infrastructure.Command;
using System.Collections.Generic;

namespace Canine.Net.Infrastructure.RabbitMQ.DTO
{
    /// <summary>
    /// Use for communicating claim requirements of a specific service.
    /// </summary>
    public class ClaimDefinitions
    {
        public ClaimDefinitions(List<Claim> definitions)
        {
            this.Definitions = definitions;
        }

        /// <summary>
        /// Gets or sets Definitions
        /// </summary>
        public List<Claim> Definitions { get; set; }
    }
}
