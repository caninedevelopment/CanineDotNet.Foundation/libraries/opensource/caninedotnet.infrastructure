﻿namespace Canine.Net.Infrastructure.RabbitMQ.DTO
{
    using System.Collections.Generic;
    using Monosoft.Common.Command;
    using Canine.Net.Infrastructure.Console;
    using Canine.Net.Infrastructure.RabbitMQ.Message;

    public class ServiceDescriptors
    {
        public List<ServiceDescriptor> Services { get; set; } = new List<ServiceDescriptor>();
    }

    public class ServiceDescriptor
    {
        public ServiceDescriptor()
        {
            this.ResourceDescriptions = new List<ResourceDescription>();
        }

        public ServiceDescriptor(ProgramVersion version, ResourceDescription resourceDescription)
        {
            this.Version = version;
            this.ResourceDescriptions = new List<ResourceDescription>() { resourceDescription };
        }

        public ServiceDescriptor(ResourceDescription resourceDescription)
        {
            this.ResourceDescriptions = new List<ResourceDescription>() { resourceDescription };
        }
        public string Name { get; set; }

        public ProgramVersion Version { get; set; }

        public List<ResourceDescription> ResourceDescriptions { get; set; }
    }
}
