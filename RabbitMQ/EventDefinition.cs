﻿namespace Canine.Net.Infrastructure.Command
{
    public interface IEventDefinitionBase
    {
        string Route { get; }
        string Description { get; }
    }

    public interface IEventDefinition<DtoType> : IEventDefinitionBase where DtoType : class
    {
        DtoType Dto();
    }
    public class EventDefinition<DtoType> : IEventDefinition<DtoType> where DtoType : class
    {
        protected string description;
        protected string route;

        public EventDefinition(string route, string description)
        {
            this.route = route;
            this.description = description;
        }

        public string Description { get { return this.description; } }
        public string Route { get { return this.route; } }
        public DtoType Dto() { return default; }
    }
}
